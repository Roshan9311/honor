import * as express from 'express'
import IServer from '../interfaces/ServerInterface'
import config from '../config/config'
import path = require('path')

export default class Routes {
    static init(server: IServer): void {
        const router: express.Router = express.Router()

        router.get('/config', (req, res) => {
			const c = {
				votingStartDate: config.votingStartDate,
				votingDuration: config.votingDuration,
				maxRewards: config.maxRewards
			}

            res.json(c)
		})

        router.get('/nutzungsbestimmungen', (req, res) => {
            res.sendFile(path.join(__dirname, '..', '..', 'public', 'html', 'nutzungsbestimmungen.html'))
        })

		router.get('/datenschutz', (req, res) => {
            res.sendFile(path.join(__dirname, '..', '..', 'public', 'html', 'datenschutz.html'))
        })

        router.get('/impressum', (req, res) => {
            res.sendFile(path.join(__dirname, '..', '..', 'public', 'html', 'impressum.html'))
        })

        server.app.use('/', router)
    }
}
