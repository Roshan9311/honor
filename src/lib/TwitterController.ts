import { TwitterService } from '.'
import * as SocketIO from 'socket.io'
import * as fs from 'fs';
import * as path from 'path';

export class TwitterController {
	hashtags: Array<string> = ['honorplay', 'crazyfast', 'crazysmart']
	service: TwitterService

	private io: SocketIO.Server

	constructor(io: SocketIO.Server) {
		this.io = io
		this.io.on('connection', this.onSocketConnected.bind(this))
	}

	init() {
		const countsString = fs.readFileSync(path.join(__dirname, 'dumb_cache')).toString()
		const counts = JSON.parse(countsString) || { honorplay: 0, crazyfast: 0, crazysmart: 0 }

		console.log(`Starting from ${countsString}`)

		this.service = new TwitterService(this.hashtags, this.onResultsUpdated.bind(this))
		this.service.init(counts)
	}

	onSocketConnected(socket) {
		console.log(`Socket ${socket.id} connected`)
		socket.emit('counts', this.service.counts)
	}

	onResultsUpdated(counts) {
		console.log('New counts -> ', counts)

		this.io.emit('counts', counts)

		const countsString = JSON.stringify(counts)
		fs.writeFile(path.join(__dirname, 'dumb_cache'), countsString, () => {})
	}

	honorCallback(err, results) {
		if (err) {
			console.error(err)
		} else {
			console.log('Honor Hashtags count:', results)
		}
	}

	playersCallback(err, results) {
		if (err) {
			console.error(err)
		} else {
			console.log('Player Hashtags count:', results)
		}
	}

	getHonorCount(req, res) {
		const hashtags = [
			'basketball'
		]

		// TODO: Get hashtags count from service
	}

	getHashtagsCount(req, res) {
		const hashtags = [
			'honorplay',
			req.params.player
		]

		console.log('Get count for...', hashtags)

		// TODO: Get hashtags count from service
    }
}
