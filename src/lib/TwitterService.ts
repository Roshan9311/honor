import { default as config } from '../config/config'
import Twit = require('twit')

const Twitter = new Twit({
	consumer_key: config.twitter.consumerKey,
	consumer_secret: config.twitter.consumerSecret,
	access_token: config.twitter.accessToken,
	access_token_secret: config.twitter.accessTokenSecret,
	timeout_ms: 60 * 1000
})

export class TwitterService {
	stream
	hashtags: Array<string>

	firstConnection = false
	connected = false
	lostConnection = false

	ids: Array<number> = new Array()

	counts = {}

	match = {
		crazyfast: 'crazyfast',
		crazysmart: 'crazysmart',
		honorplay: 'honorplay'
	}

	onResultChanged: Function

	constructor(hashtags: Array<string>, onResultChanged: Function) {
		this.hashtags = hashtags
		this.onResultChanged = onResultChanged
	}

	init(counts) {
		this.counts = counts

		// this.hashtags.forEach(h => {
		// 	this.counts[h] = 0
		// })

		this.onResultChanged(this.counts)

		const prefixedHashtags = this.hashtags.map(h => '#' + h)

		console.log(prefixedHashtags)
		this.stream = Twitter.stream('statuses/filter', {
			track: prefixedHashtags
		})

		this.stream.on('parser-error', this.onParserError.bind(this))
		this.stream.on('connect', this.onConnect.bind(this))
		this.stream.on('reconnect', this.onReconnect.bind(this))
		this.stream.on('connected', this.onConnected.bind(this))
		this.stream.on('error', this.onError.bind(this))
		this.stream.on('tweet', this.onTweet.bind(this))
		this.stream.on('limitation', (tweet) => {
			console.log('limitation', tweet)
		})
	}

	onTweet(tweet) {
		if (this.ids.indexOf(tweet.id) > -1) return

		if (tweet.text.toLowerCase().indexOf())

		var htSources = []

		var htTerms = []

		htSources.push(tweet.entities.hashtags)

		if (tweet.retweeted_status !== undefined) {
			htSources.push(tweet.retweeted_status.entities.hashtags)

			if (tweet.retweeted_status.extended_tweet !== undefined) {
				htSources.push(tweet.retweeted_status.extended_tweet.entities.hashtags)
			}
		}

		if (tweet.quoted_status !== undefined) {
		  	htSources.push(tweet.quoted_status.entities.hashtags)
		}

		htSources.forEach((source) => {
			if (source !== undefined) {
				var newHashtags = source.map((hashtagObj) => {
					return hashtagObj.text.toLowerCase()
				})

				htTerms = htTerms.concat(newHashtags)
			}
		})

		this.ids.push(tweet.id)

		if (htTerms.length < 2) return

		if ((htTerms.indexOf(this.match.crazyfast) > -1 && htTerms.indexOf(this.match.honorplay) > -1) ||
			(htTerms.indexOf(this.match.crazysmart) > -1 && htTerms.indexOf(this.match.honorplay) > -1)) {
			this.hashtags.forEach((searchHashtag) => {
				var searchTerm = searchHashtag.toLowerCase()

				if (htTerms.indexOf(searchTerm) !== -1) {
					this.counts[searchHashtag] += 1
				}
			})

			this.onResultChanged(this.counts)
		}
	}

	onParserError(error: Error) {
		console.error('onParserError', error)
	}

	onConnect() {
		console.log('onConnect')

		if (this.firstConnection === true) {
			// TODO: Do something on connect

			this.firstConnection = false
		}
	}

	onReconnect(d) {
		console.log('onReconnect', d)

		if (this.lostConnection === false && this.firstConnection === false) {


			this.lostConnection = true
		}

		this.connected = false
	}

	onConnected() {
		console.log('onConnected')

		this.connected = true
		this.lostConnection = false
	}

	onError(error: Error) {
		console.error('onError', error)
	}
}
