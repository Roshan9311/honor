import IServer from '../interfaces/ServerInterface'
import * as SocketIO from 'socket.io'

import { TwitterController } from '.'

export class Twitter {
	controller: TwitterController

    static init(server: IServer): void {
        const ioServer = require('http').Server(server.app)
        ioServer.listen(8080)

		const io: SocketIO.Server = SocketIO(ioServer)
		const controller: TwitterController = new TwitterController(io)

        controller.init()
    }
}
