import * as express from 'express'
import Routes from './router/routes'
import Middleware from './config/middleware'
import { Twitter } from './lib'

export class Server {
    public app: express.Application

    constructor() {
        this.app = express()
        Middleware.init(this)
        Routes.init(this)
        Twitter.init(this)
    }
}

export default new Server().app
