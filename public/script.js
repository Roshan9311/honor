function play(element) {
	const iframe = document.getElementById("frame");
	const link = "https://www.youtube.com/embed/oHg5SJYRHA0?enable_js=1&autoplay=1";

	element.setAttribute("class", "hidden");
	iframe.setAttribute("class", "shown");
	iframe.setAttribute("src", link);
}

(function(win) {
	win.onload = () => {
		let started = false
		let interval

		const updateCounts = (config, counts) => {
			const maxRewards = parseInt(config.maxRewards);

			const hp = parseInt(counts.honorplay);
			const cf = parseInt(counts.crazyfast);
			const cs = parseInt(counts.crazysmart);

			const chicken = $('.top-progress-chicken');

			if (chicken) {
				let percentage = `0%`;
				const total = cs + cf;

				if (cf >= cs) {
					percentage = `-${parseInt('' + ((cf * 100) / total))}%`;
				} else {
					percentage = `${parseInt('' + ((cs * 100) / total))}%`;
				}

				chicken.css('transform', `translateX(${percentage}) translateY(-7.2vw)`);
			}

			const progress = $('.prize-progress-fill');

			if (progress) {
				const maxSize = 64.8;
				let size = parseInt('' + ((hp * maxSize) / maxRewards));

				if (size < 0) {
					size = 0;
				} else if (size > maxSize) {
					size = maxSize;
				}

				console.log(size);

				progress.css('width', `${size}vw`);
				console.log(hp)
				if (hp >= maxRewards * 0.25) {
					$('.prize-progress-plus-1').css('display', 'unset');
				} else {
					$('.prize-progress-plus-1').css('display', 'none');
				}

				if (hp >= maxRewards * 0.5) {
					$('.prize-progress-plus-2').css('display', 'unset');
				} else {
					$('.prize-progress-plus-2').css('display', 'none');
				}

				if (hp >= maxRewards * 0.75) {
					$('.prize-progress-plus-3').css('display', 'unset');
				} else {
					$('.prize-progress-plus-3').css('display', 'none');
				}

				if (hp >= maxRewards) {
					$('.prize-progress-plus-4').css('display', 'unset');
				} else {
					$('.prize-progress-plus-4').css('display', 'none');
				}
			}
		}

		const init = (config) => {
			const socket = io.connect(win.location.hostname + ':8080');

			startCounting(config);

			interval = setInterval(() => {
				startCounting(config);
			}, 1000);

			socket.on('counts', (counts) => {
				if (!counts) {
					return;
				}

				updateCounts(config, counts);
			});
		}

		const startCounting = (config) => {
			const now = new Date();

			const starts = new Date(config.votingStartDate);
			const duration = parseInt(config.votingDuration);

			const difference = duration - Math.abs(now.getTime() - starts.getTime());

			const difSeconds = parseInt('' + difference / 1000);
			const difMinutes = parseInt('' + difSeconds / 60);
			const difHours = parseInt('' + difMinutes / 60);

			const pad = (n) => {
				return n < 10 ? '0' + n : n;
			}

			const hours = pad(difHours);
			const minutes = pad(difMinutes % 60);
			const seconds = pad(difSeconds % 60);

			$('.top-timer').text(`${hours}:${minutes}:${seconds}`);

			started = starts.getTime() <= now.getTime() && (difference <= duration) && difference >= 0;

			if (started) {
				$('.to-be-shown').css('display', 'flex');
			} else {
				$('.to-be-shown').css('display', 'none');
			}
		}

		(async () => {
			$.get('/config').then(config => {
				init(config)
			}).catch(error => {
				console.error(error)
			});
		})();
	};
})(window);
